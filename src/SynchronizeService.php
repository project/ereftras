<?php

namespace Drupal\ereftras;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class SynchronizeService.
 */
class SynchronizeService implements ContainerAwareInterface {

  use ContainerAwareTrait;
  use StringTranslationTrait;

  /**
   * Create translated field for referenced entities.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The node type.
   * @param array $field_names
   *   The reference field names.
   * @param bool $only_empty
   *   Flag for non empty values overwrite.
   */
  public function synchronize(string $entity_type = NULL,
    string $bundle = NULL,
    array $field_names = [],
    bool $only_empty = TRUE) {

    if (empty($entity_type) || empty($bundle) || empty($field_names)) {
      // No required parameters get.
      return;
    }

    $operations = [];
    $entities = $this->container->get('entity_type.manager')
      ->getStorage($entity_type)->loadByProperties(['type' => $bundle]);
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    foreach ($entities as $entity) {
      $translations = $entity->getTranslationLanguages();
      $translatable_fields = $entity->getTranslatableFields();
      foreach (array_keys($translations) as $language_code) {
        if ($entity->hasTranslation($language_code)) {
          if ($translated_entity = $entity->getTranslation($language_code)) {
            $changed = FALSE;
            foreach ($field_names as $field_name) {
              if (!isset($translatable_fields[$field_name])) {
                continue;
              }
              $original_value = $entity->get($field_name)->getValue();
              $value = $translated_entity->get($field_name)->getValue();
              if ((!empty($original_value) && empty($value)) ||
                (!empty($original_value) && $only_empty === FALSE)) {
                $translated_entity->get($field_name)->setValue($original_value);
                $changed = TRUE;
              }
            }
            if ($changed === TRUE) {
              $operations[] = [
                '\Drupal\ereftras\SynchronizeService::saveEntities',
                [$translated_entity],
              ];
            }
          }
        }
      }
    }

    if (!empty($operations)) {
      // Set batch process.
      $batch = [
        'title' => $this->t('Entities save ...'),
        'operations' => $operations,
        'finished' => '\Drupal\ereftras\SynchronizeService::saveEntitiesFinishedCallback',
      ];
      batch_set($batch);
    }
  }

  /**
   * Batch operation callback.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The translated entity.
   * @param array $context
   *   The context array.
   */
  public static function saveEntities(EntityInterface $entity,
    array &$context) {
    if ($entity instanceof EntityInterface) {
      $context['message'] = \Drupal::translation()
        ->translate('Save: @label', ['@label' => $entity->label()]);
      $entity->save();
      $context['results'][] = $entity->id();
    }
  }

  /**
   * Batch finish callback.
   *
   * @param string $success
   *   The result string.
   * @param array $results
   *   The results array.
   * @param array $operations
   *   The unprocessed array.
   */
  public static function saveEntitiesFinishedCallback($success,
    array $results,
    array $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One translation saved.', '@count translations saved.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }

    \Drupal::service('messenger')->addMessage($message);
  }

}
