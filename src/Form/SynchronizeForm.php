<?php

namespace Drupal\ereftras\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;

/**
 * Class SynchronizeForm.
 */
class SynchronizeForm extends FormBase {

  /**
   * Symfony\Component\DependencyInjection\ContainerInterface definition.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructs a new SynchronizeForm object.
   */
  public function __construct(
    ContainerInterface $database
  ) {
    $this->container = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synchronize_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,
    FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    $entity_types = $entity_type_manager->getDefinitions();
    $labels = [$this->t('Select entity type')];
    $content_bundle_fields = [];

    $bundles = $this->container->get('entity_type.bundle.info')
      ->getAllBundleInfo();

    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type */
    foreach ($entity_types as $entity_type_id => $entity_type) {
      /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
      $storage = $entity_type_manager->getStorage($entity_type_id);
      $type = $storage->getEntityType();
      if (!$entity_type instanceof ContentEntityTypeInterface || !$entity_type->hasKey('langcode') || !isset($bundles[$entity_type_id])) {
        continue;
      }
      foreach ($bundles[$entity_type_id] as $bundle_name => $bundle) {
        $field_definitions = $this->container->get('entity_field.manager')
          ->getFieldDefinitions($entity_type_id, $bundle_name);
        $fields = [];
        /** @var \Drupal\Core\Field\BaseFieldDefinition $definition */
        foreach ($field_definitions as $name => $definition) {
          $t = $definition->getFieldStorageDefinition()->getType();
          if ((!$definition->isComputed()) &&
            $definition->isTranslatable() &&
            $definition->getFieldStorageDefinition()->getType() === 'entity_reference') {
            $fields[$name] = $definition->getLabel();
          }
        }
        if (!empty($fields)) {
          $content_bundles[$entity_type_id][$bundle_name] = $bundle['label'];
          $content_bundle_fields[$entity_type_id][$bundle_name] = $fields;
        }
      }
      if (empty($content_bundle_fields[$entity_type_id])) {
        continue;
      }

      $labels[$entity_type_id] = $entity_type->getLabel() ?: $entity_type_id;
    }

    $input = $form_state->getUserInput();
    $type = $form_state->getValue('entity_types');
    $bundle = $form_state->getValue('bundle');
    $fields = $form_state->getValue('fields');

    $form['entity_types'] = [
      '#title' => $this->t('Entity type'),
      '#type' => 'select',
      '#options' => $labels,
      '#default_value' => $type,
      '#ajax' => [
        'callback' => '::bundleCallback',
        'wrapper' => 'bundle-wrapper',
        'disable-refocus' => FALSE,
        'event' => 'change',
      ],
    ];

    $form['bundle_wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="bundle-wrapper">',
      '#suffix' => '</div>',
    ];
    if (!empty($content_bundles[$type])) {
      $bundle_options = [$this->t('Select bundle')];
      $bundle_options += $content_bundles[$type];
      $form['bundle_wrapper']['bundle'] = [
        '#title' => $this->t('Bundle'),
        '#type' => 'select',
        '#options' => $bundle_options,
        '#default_value' => $bundle,
        '#ajax' => [
          'callback' => '::fieldsCallback',
          'wrapper' => 'field-wrapper',
          'disable-refocus' => FALSE,
          'event' => 'change',
        ],
      ];
    }

    $form['field_wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="field-wrapper">',
      '#suffix' => '</div>',
    ];
    if (!empty($content_bundle_fields[$type][$bundle])) {
      $field_options = $content_bundle_fields[$type][$bundle];
      $form['field_wrapper']['fields'] = [
        '#title' => $this->t('Fields'),
        '#type' => 'select',
        '#options' => $field_options,
        '#default_value' => $fields,
        '#multiple' => TRUE,
      ];
    }

    $form['non_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Synchronize non empty values too'),
      '#description' => $this->t('Will synchronize all translation entity reference value with original entity field value not just empty values.'),
      '#default_value' => 0,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form,
    FormStateInterface $form_state) {
    $type = $form_state->getValue('entity_types');
    $bundle = $form_state->getValue('bundle');
    $fields = $form_state->getValue('fields');
    $non_empty = !$form_state->getValue('non_empty');
    $this->container->get('ereftras.synchronize')
      ->Synchronize($type, $bundle, $fields, $non_empty);
  }

  /**
   * The form entity type element ajax callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form element.
   */
  public function bundleCallback(array &$form,
    FormStateInterface $form_state) {
    return $form['bundle_wrapper'];
  }

  /**
   * The form bundle element ajax callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form element.
   */
  public function fieldsCallback(array &$form,
    FormStateInterface $form_state) {
    return $form['field_wrapper'];
  }

}
