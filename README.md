Entity Reference Fields Translation Synchronize
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
This module implements a synchronizing function if translations are
for entity type but needs to enable some entity reference field translation.

REQUIREMENTS
------------
This module requires Drupal Content Translation core module.

INSTALLATION
------------
Install the Entity Reference Field Translation Synchronize module as you would
typically install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
--------------
In site translations exists for entity type but needs to enable some entity
reference field translation. This operation will enable the field translation
but don't populate translated entities original referenced field values.
This module prepare a synchronize fieldset on field configuration form.
Translations entity reference field values will synchronizing with original
entity reference field values if enabled synchronization.

   1. Navigate to Administration > Extend and enable the Entity Reference Field
      Translation Synchronize module.

   2. Navigate some entity field settings form and edit any entity reference
      field.

   3. Enable the "Users may translate this field" checkbox.

   4. Enable the "Synchronize existing translations with original value" in
      "Entity reference field translation synchronizes" fieldset.

   5. Optionally check the "Overwrite existing translated value with original
      value" checkbox this option enables every existing entity reference value
      resynchronizing.

   6. Save settings.

If you need multiple reference field synchronizing go to 
Administration > Config > Development > Entity Reference Field Translation
Synchronize and choose the Entity type, Bundle and fields you want to
synchronize, optionally check the synchronize non-empty values too checkbox and
submit form.

MAINTAINERS
-----------

The 8.x-1.x branch was created by:

 * Dudas Jozsef (dj1999) - https://www.drupal.org/u/dj1999

This module was created and sponsored by Brainsum, a drupal development company
in Budapest, Hungary.

 * Brainsum - https://www.brainsum.com/
